app-container-nmap
=======================


build local container
=====================

```
pushd local_scripts/
```
```
./docker_build.sh
```
```
popd
```

run local container with ash
============================

```
pushd local_scripts/
```

```
./docker_run-ash.sh reslocal/app-container-nmap
```

```
...
+ ID=$(docker run -t -i -d reslocal/app-container-nmap ash -l)
+ ID 38cb4e887907f42ac6cc1f4b5e39b847b9aaaec68efeb153cf668ccbcdad604d
+ docker attach 38cb4e887907f42ac6cc1f4b5e39b847b9aaaec68efeb153cf668ccbcdad604d
...
```

```
root@38cb4e887907:/# nmap -v -sn 192.168.42.1
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-15 12:34 UTC
Initiating Ping Scan at 12:34
Scanning 192.168.42.1 [4 ports]
Completed Ping Scan at 12:34, 0.22s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 12:34
Completed Parallel DNS resolution of 1 host. at 12:34, 0.00s elapsed
Nmap scan report for e450-1.res.training (192.168.42.1)
Host is up (0.000084s latency).
Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.25 seconds
           Raw packets sent: 4 (152B) | Rcvd: 1 (28B)
```

```
root@38cb4e887907:/# exit
```

```
popd
```


run without ash
===============

```
pushd local_scripts/
```
```
./docker_run.sh reslocal/app-container-nmap nmap -v -sn 192.168.42.1
```

```
...
+ ID=$(docker run -t -i reslocal/app-container-nmap nmap -v -sn 192.168.42.1)
+ ID Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-15 12:37 UTC
Initiating Ping Scan at 12:37
Scanning 192.168.42.1 [4 ports]
Completed Ping Scan at 12:37, 0.21s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 12:37
Completed Parallel DNS resolution of 1 host. at 12:37, 0.00s elapsed
Nmap scan report for e450-1.res.training (192.168.42.1)
Host is up (0.000087s latency).
Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.24 seconds
           Raw packets sent: 4 (152B) | Rcvd: 1 (28B)
```
```
popd
```

